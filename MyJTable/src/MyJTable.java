import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class MyJTable extends JTable {
	
	public MyJTable(int x, int y) {
	this.setModel(new DefaultTableModel(x,y));
	}
	
	public MyJTable(Object[][] tab) {
	int x=tab.length;
	int y=tab[0].length;
	
	this.setModel(new DefaultTableModel(x,y));
	
	setValues(tab);
	}
	
	
	public void setValues(Object[][] tab) {
		int x=tab.length;
		int y=tab[0].length;
		
		this.setModel(new DefaultTableModel(x,y));
		
		for (int c=0;c<tab.length;c++) {
			for (int d=0;d<tab[0].length;d++) {
				this.setValueAt(tab[c][d], c, d);
			}
		}
		clear();
	}
	
	public Object[][] toTab(){
		int x=this.getRowCount();
		int y=this.getColumnCount();
		
		Object[][] res=new Object[x][y];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				res[c][d]=this.getValueAt(c, d);
			}
		}
		return res;
	}
	
	public void removeRow(int index) {
		Object[][] tab=this.toTab();
		int x=tab.length;
		int y=tab[0].length;
		
		Object[][] res=new Object[x-1][y];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				if (c<index) {
					res[c][d]=tab[c][d];
				} else {
					if (c>index) {
						res[c-1][d]=tab[c][d];
					}
				}
			}
		}
		this.setValues(res);
	}
	
	public void removeColumn(int index) {
		Object[][] tab=this.toTab();
		int x=tab.length;
		int y=tab[0].length;
		
		Object[][] res=new Object[x][y-1];
		
		for (int c=0;c<x;c++) {
			for (int d=0;d<y;d++) {
				if (d<index) {
					res[c][d]=tab[c][d];
				} else {
					if (d>index) {
						res[c][d-1]=tab[c][d];
					}
				}
			}
		}
		this.setValues(res);
	}
	
	public void clear(){
		Object[][] clean=this.toTab();
		
		for (int c=0;c<9;c++) {
			for (int d=0;d<9;d++) {
				if ((int)clean[c][d]==0) {
					this.setValueAt("", c, d);
				}
			}
		}
	}

}
